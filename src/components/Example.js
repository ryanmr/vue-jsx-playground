const hello = (h, name) => <h2>Hello, {name}!</h2>

export default {
  name: 'example',
  data () {
    return {
      name: 'example name'
    }
  },
  methods: {
    input (prop, value) {
      this[prop] = value
    }
  },
  render (h) {
    return (
      <div>
        {hello(h, this.name)}
        <input type="text" on-input={(e) => this.input('name', e.target.value)} value={this.name} />
      </div>
    )
  }
}
