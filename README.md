# vue-jsx

> An example of Vue with a JSX component.

## Sources

I following the pattern provided by [JSX Render Functions](https://alligator.io/vuejs/jsx-render-functions/). I used the [babel-plugin-transform-vue-jsx](https://github.com/vuejs/babel-plugin-transform-vue-jsx).

## Thoughts

In my opinion, mutating data locally feels much more natural by assignment `this.prop = value`, rather than React's `this.setState({prop: value})`. That's one of my favorite parts about Vue.

One of my favorite parts about React, are simple functional stateless components. They look like this:

```js
const hello = name => <h2>Hello, {name}!</h2>
``` 

Making these tiny little functions that take in a tiny piece of parent stat and return a tiny fragment of interface is wonderful. In Vue, making these tiny little components feels frustrating beacuse of the perceived boilerplate that comes with making an entire Vue component.

In Vue with JSX, these tiny functional components can exist just like the React snippet, but with an extra `h` parameter, which aliases Vue's `$createElement` method. It just has to be in scope, it doesn't need to be used in the actual function.

```js
const hello = (h, name) => <h2>Hello, {name}!</h2>
``` 

The transform will turn that into something like this:

```js
var c = function(t, e) {
        return t("h2", null, ["Hello, ", e, "!"])
```

The transformation is pretty good. Vue with JSX is not perfect. For example, everyone's favorite _two way binding_ feature, `v-model` does not work out of the box. Good thing adding events like `on-input` are very easy in the JSX markup.

```jsx
<input type="text" value={this.name}
  on-input={(e) => this.input('name', e.target.value)} />
```

Along with the usual Vue data and methods setup:

```js
data () {
  return {
    name: 'example name'
  }
},
methods: {
  input (prop, value) {
    this[prop] = value
  }
},
```

Is Vue with JSX better than templating? I would argue on something form heavy, where `v-model` shines the best, no. But on something that is display oriented, something with a lot of tiny little composable variances, Vue with JSX works well.

I look forward to more Vue interop with JSX style approaches to functional component compositon.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
